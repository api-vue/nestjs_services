// @generated by protobuf-ts 2.9.0
// @generated from protobuf file "notification/v1alpha/service.proto" (package "notification", syntax proto3)
// tslint:disable
import type { RpcTransport } from "@protobuf-ts/runtime-rpc";
import type { ServiceInfo } from "@protobuf-ts/runtime-rpc";
import { NotificationService } from "./service";
import type { SendNotificationResponse } from "./service";
import type { SendNotificationRequest } from "./service";
import type { DeleteNotificationResponse } from "./service";
import type { DeleteNotificationRequest } from "./service";
import type { AddNotificationResponse } from "./service";
import type { AddNotificationRequest } from "./service";
import { stackIntercept } from "@protobuf-ts/runtime-rpc";
import type { GetNotificationResponse } from "./service";
import type { GetNotificationRequest } from "./service";
import type { UnaryCall } from "@protobuf-ts/runtime-rpc";
import type { RpcOptions } from "@protobuf-ts/runtime-rpc";
/**
 * @generated from protobuf service notification.NotificationService
 */
export interface INotificationServiceClient {
    /**
     * @generated from protobuf rpc: GetNotification(notification.GetNotificationRequest) returns (notification.GetNotificationResponse);
     */
    getNotification(input: GetNotificationRequest, options?: RpcOptions): UnaryCall<GetNotificationRequest, GetNotificationResponse>;
    /**
     * @generated from protobuf rpc: AddNotification(notification.AddNotificationRequest) returns (notification.AddNotificationResponse);
     */
    addNotification(input: AddNotificationRequest, options?: RpcOptions): UnaryCall<AddNotificationRequest, AddNotificationResponse>;
    /**
     * @generated from protobuf rpc: DeleteNotification(notification.DeleteNotificationRequest) returns (notification.DeleteNotificationResponse);
     */
    deleteNotification(input: DeleteNotificationRequest, options?: RpcOptions): UnaryCall<DeleteNotificationRequest, DeleteNotificationResponse>;
    /**
     * @generated from protobuf rpc: SendNotification(notification.SendNotificationRequest) returns (notification.SendNotificationResponse);
     */
    sendNotification(input: SendNotificationRequest, options?: RpcOptions): UnaryCall<SendNotificationRequest, SendNotificationResponse>;
}
/**
 * @generated from protobuf service notification.NotificationService
 */
export class NotificationServiceClient implements INotificationServiceClient, ServiceInfo {
    typeName = NotificationService.typeName;
    methods = NotificationService.methods;
    options = NotificationService.options;
    constructor(private readonly _transport: RpcTransport) {
    }
    /**
     * @generated from protobuf rpc: GetNotification(notification.GetNotificationRequest) returns (notification.GetNotificationResponse);
     */
    getNotification(input: GetNotificationRequest, options?: RpcOptions): UnaryCall<GetNotificationRequest, GetNotificationResponse> {
        const method = this.methods[0], opt = this._transport.mergeOptions(options);
        return stackIntercept<GetNotificationRequest, GetNotificationResponse>("unary", this._transport, method, opt, input);
    }
    /**
     * @generated from protobuf rpc: AddNotification(notification.AddNotificationRequest) returns (notification.AddNotificationResponse);
     */
    addNotification(input: AddNotificationRequest, options?: RpcOptions): UnaryCall<AddNotificationRequest, AddNotificationResponse> {
        const method = this.methods[1], opt = this._transport.mergeOptions(options);
        return stackIntercept<AddNotificationRequest, AddNotificationResponse>("unary", this._transport, method, opt, input);
    }
    /**
     * @generated from protobuf rpc: DeleteNotification(notification.DeleteNotificationRequest) returns (notification.DeleteNotificationResponse);
     */
    deleteNotification(input: DeleteNotificationRequest, options?: RpcOptions): UnaryCall<DeleteNotificationRequest, DeleteNotificationResponse> {
        const method = this.methods[2], opt = this._transport.mergeOptions(options);
        return stackIntercept<DeleteNotificationRequest, DeleteNotificationResponse>("unary", this._transport, method, opt, input);
    }
    /**
     * @generated from protobuf rpc: SendNotification(notification.SendNotificationRequest) returns (notification.SendNotificationResponse);
     */
    sendNotification(input: SendNotificationRequest, options?: RpcOptions): UnaryCall<SendNotificationRequest, SendNotificationResponse> {
        const method = this.methods[3], opt = this._transport.mergeOptions(options);
        return stackIntercept<SendNotificationRequest, SendNotificationResponse>("unary", this._transport, method, opt, input);
    }
}
