/* eslint-disable */
import { Metadata } from "@grpc/grpc-js";
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";

export const protobufPackage = "notification";

export interface Notification {
  title: string;
  body: string;
  recipients: string[];
  timestamp: number;
}

export interface GetNotificationRequest {
  id: string;
}

export interface GetNotificationResponse {
  notification: Notification | undefined;
}

export interface AddNotificationRequest {
  notification: Notification | undefined;
}

export interface AddNotificationResponse {
  id: string;
}

export interface DeleteNotificationRequest {
  id: string;
}

export interface DeleteNotificationResponse {
  success: boolean;
}

export interface SendNotificationRequest {
  id: string;
}

export interface SendNotificationResponse {
  success: boolean;
}

export const NOTIFICATION_PACKAGE_NAME = "notification";

export interface NotificationServiceClient {
  getNotification(request: GetNotificationRequest, metadata?: Metadata): Observable<GetNotificationResponse>;

  addNotification(request: AddNotificationRequest, metadata?: Metadata): Observable<AddNotificationResponse>;

  deleteNotification(request: DeleteNotificationRequest, metadata?: Metadata): Observable<DeleteNotificationResponse>;

  sendNotification(request: SendNotificationRequest, metadata?: Metadata): Observable<SendNotificationResponse>;
}

export interface NotificationServiceController {
  getNotification(
    request: GetNotificationRequest,
    metadata?: Metadata,
  ): Promise<GetNotificationResponse> | Observable<GetNotificationResponse> | GetNotificationResponse;

  addNotification(
    request: AddNotificationRequest,
    metadata?: Metadata,
  ): Promise<AddNotificationResponse> | Observable<AddNotificationResponse> | AddNotificationResponse;

  deleteNotification(
    request: DeleteNotificationRequest,
    metadata?: Metadata,
  ): Promise<DeleteNotificationResponse> | Observable<DeleteNotificationResponse> | DeleteNotificationResponse;

  sendNotification(
    request: SendNotificationRequest,
    metadata?: Metadata,
  ): Promise<SendNotificationResponse> | Observable<SendNotificationResponse> | SendNotificationResponse;
}

export function NotificationServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["getNotification", "addNotification", "deleteNotification", "sendNotification"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("NotificationService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("NotificationService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const NOTIFICATION_SERVICE_NAME = "NotificationService";
